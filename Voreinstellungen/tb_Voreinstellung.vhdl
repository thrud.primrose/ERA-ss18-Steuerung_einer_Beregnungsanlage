library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;


entity tb_Voreinstellung is
end entity;

architecture tb1 of tb_Voreinstellung is

component Voreinstellung is
	port (
		FuehlerBedarf: in std_logic;
        SekundenEinerBCD: in std_logic_vector(3 downto 0);
        SekundenZehnerBCD: in std_logic_vector(3 downto 0);
        MinutenEinerBCD: in std_logic_vector(3 downto 0);
        Sekundenvorgabe: out unsigned(11 downto 0)
	);
end component Voreinstellung;

signal FuehlerBedarf1: std_logic :='1';

    signal   SchalterSekundenEinerBCD1: std_logic_vector(3 downto 0):="0000";
   
signal SchalterSekundenZehnerBCD1: std_logic_vector(3 downto 0):="0000";

    signal SchalterMinutenBCD1: std_logic_vector(3 downto 0):="0000";

signal Sekundenvorgabe1: unsigned(11 downto 0);

subtype lol is integer range 0 to 666666;
signal minuteintern: lol:=0;
signal sekzehnerintern: lol:=0;
signal sekeinerintern: lol:=0;
signal counter: lol:=0;
signal sek: std_logic:='0';
signal ende: std_logic := '0';


begin

Voreinstellung1: Voreinstellung port map (
FuehlerBedarf=> FuehlerBedarf1, 
MinutenEinerBCD => SchalterMinutenBCD1, 
SekundenEinerBCD => SchalterSekundenEinerBCD1,
SekundenZehnerBCD => SchalterSekundenZehnerBCD1,
Sekundenvorgabe => Sekundenvorgabe1
);

--Sekundentakt
process
begin 
if(counter<600) then
sek <= not sek;
end if;
wait for 0.5 sec;
end process;

--Jede Sekunden wird der Counter erhöht bis er 600 erreicht hat
process(sek)
begin
if(rising_edge(sek) and counter<600) then
  counter <= counter +1; 
  end if;
end process;

--Jede Sekunde wird die bisher vergangene Zeit in den BCD Code umgerechnet
sekeinerintern <= (counter mod 10);
sekzehnerintern <= ((counter mod  60)/10);
minuteintern <= (counter/60);

--Nach 600 Sekunden ist die Simulation vorbei und es wird 0 weitergegeben
SchalterMinutenBCD1 <= std_logic_vector((to_unsigned(minuteintern,4))) when (counter<600) else "0000";
SchalterSekundenEinerBCD1 <= std_logic_vector((to_unsigned(sekeinerintern,4)))when (counter<600) else "0000";
SchalterSekundenZehnerBCD1<= std_logic_vector((to_unsigned(sekzehnerintern,4)))when (counter<600) else "0000";


--Falls ein Wert weitergegeben wird obwohl der Fühler keinen Bedarf zeigt oder der falsche Wert weitergegeben wird, gibt es eine Felhermeldung 
process(sek)
begin
if(falling_edge(sek)) then
if(counter<600) then
if(FuehlerBedarf1='1') then
  assert (Sekundenvorgabe1 = to_unsigned(counter,12)) report "Fehler! Falscher Wert an Sekundenvorgabe. Dishonor, failure, commit sudoku!"&integer'image(to_integer(Sekundenvorgabe1))&" and "&integer'image(counter) severity error;
else
  assert (Sekundenvorgabe1 = to_unsigned(0,12)) report "Fehler! Falscher Wert an Sekundenvorgabe. Dishonor, failure, commit sudoku!"&integer'image(to_integer(Sekundenvorgabe1))&" and "&integer'image(counter) severity error;    
end if;
end if;
end if;
end process;


--Für den Test wechselt alle 15 Sekunden der Fuehlerbedarf
process
begin 
if(counter<600) then
FuehlerBedarf1 <= not FuehlerBedarf1;
end if;
if(counter>=600) then
FuehlerBedarf1 <= '0';
end if;
wait for 15 sec;
end process;


--Hilfssignal zum Beenden der Simulation
ende <= '0' when counter<600 else '1';

--Ende der Simulation
process(ende)
begin
if(rising_edge(ende)) then
report "Test erfolgreich!";
end if;
end process;

end architecture;


