library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Voreinstellung is
    port (
        FuehlerBedarf       : in std_logic;

        --Schalter im BCD Code--
        SekundenEinerBCD    : in std_logic_vector(3 downto 0);
        SekundenZehnerBCD   : in std_logic_vector(3 downto 0);
        MinutenEinerBCD     : in std_logic_vector(3 downto 0);

        --Zeitvorgabe in Sekunden--
        Sekundenvorgabe     : out unsigned(11 downto 0)
    );
end entity Voreinstellung;


architecture v of Voreinstellung is

	--Hilfssignale für Umrechnungen und Fehlerbehandlung
    signal SekundenEinerIntern  : unsigned(3 downto 0) := to_unsigned(0,4);
    signal SekundenZehnerIntern : unsigned(3 downto 0) := to_unsigned(0,4);
    signal MinutenEinerIntern   : unsigned(3 downto 0) := to_unsigned(0,4);
begin
    
    --Falls ein Schalter einen grösseren Wert als den Maximalwert zeigt, wird die Eingabe auf den Maximalwert abgerundet
    SekundenEinerIntern <= "1001" when unsigned(SekundenEinerBCD) > "1001" else unsigned(SekundenEinerBCD);
    SekundenZehnerIntern <= "0101" when unsigned(SekundenZehnerBCD) > "0101" else unsigned(SekundenZehnerBCD);
    MinutenEinerIntern <= "1001" when unsigned(MinutenEinerBCD) > "1001" else unsigned(MinutenEinerBCD);
    


    --Fehlerausgabe-----------------------------------------------------
    process(SekundenEinerBCD)
        begin
        if(unsigned(SekundenEinerBCD) > "1001" )then
        report "Eingabe falsch, SekundenEiner ist groesser als 9.";
        report "SekundenEiner wird zu 9.";
        end if;
    end process;

    process(SekundenZehnerBCD)
        begin
        if(unsigned(SekundenZehnerBCD) > "0101" )then
        report "Eingabe falsch, SekundenZehner ist groesser als 5.";
        report "SekundenZehner wird zu 5.";
        end if;
    end process;

    process(MinutenEinerBCD)
        begin
        if(unsigned(MinutenEinerBCD) > "1001" )then
        report "Eingabe falsch, MinutenEiner ist groesser als 9.";
        report "MinutenEiner wird zu 9.";
        end if;
    end process;
	---------------------------------------------------------------------



	--Umrechnung
    Sekundenvorgabe <= resize(unsigned(SekundenEinerIntern) +
        (unsigned(SekundenZehnerIntern) * to_unsigned(10, 4)) +
        (unsigned(MinutenEinerIntern) * to_unsigned(60, 6)), 12) when
         (FuehlerBedarf='1' or rising_edge(FuehlerBedarf)) else to_unsigned(0,12);
    
end architecture v;



-- vim:sts=4:sw=4:et:
