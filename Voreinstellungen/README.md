# Voreinstellungen 1-4

## Voraussetzungen

Folgendes muss installiert sein:

* Linux
* GNU Make
* GHDL
* GTKWave

## Kompilieren

    $ make

## Testen

    $ make test

oder

    $ make gtkwave

## Generierungsdateien löschen

    $ make clean
