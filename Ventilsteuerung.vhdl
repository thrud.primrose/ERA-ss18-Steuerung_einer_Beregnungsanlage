library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;


entity Ventilsteuerung is
    port(
        --Die Dauer der Ventilöffnung in Sekunden (Maximal eine Stunde)
        SekundenVorgabe : in unsigned(11 downto 0);
        
        --Das vorhergehende Modul hat keinen Bedarf
        FreigabeIn      : in std_logic;

        --Wasser ist vorhanden
        WIV             : in std_logic;

        ---Takteingänge---
        Sek             : in std_logic;
        STD             : in std_logic;
    
        --Freigabesignal an das nächte Modul
        FreigabeOut     : out std_logic :='0';

        --Steuersignal für Ventil
        EMV             : out std_logic :='0'
        

    );
    
end Ventilsteuerung;


architecture Hauptventilsteuerung of Ventilsteuerung is

signal resttimeintern: positive := 1;
signal resttimeconstant: positive;
signal done: std_logic := '0';
begin



process(STD)
    begin
    if(rising_edge(STD)) then
        if(FreigabeIn='1' or rising_edge(FreigabeIn)) then
        resttimeconstant <= to_integer(SekundenVorgabe);
        end if;
    end if;
end process;
 

process(Sek)
    begin
    if(rising_edge(Sek)) then
        if(resttimeconstant>1) then
            if(resttimeintern < resttimeconstant +1) then
                if(WIV='1') then
                resttimeintern <= resttimeintern +1;
                end if;
            end if;
        end if;
    end if;
end process;


        done <= '1' when resttimeintern >= resttimeconstant else 
                '0'; 

        --emv = 0 close
        --emv = 1 is open
        EMV <= '1' when (WIV = '1' or rising_edge(WIV)) and
               (done = '0' or falling_edge(done)) and
               (FreigabeIn = '1' or rising_edge(FreigabeIn)) and
               (resttimeconstant > 1) else '0';


        --frout = 1 : next one can work
        --frout = 0 : work in progress
        FreigabeOut <=  '0' when (WIV = '0' or falling_edge(WIV)) else
                        '1' when done = '0' else
                        '1' when falling_edge(done) else
                        '0' when done = '1' else
                            '0'; 

end Hauptventilsteuerung;


architecture VentilsteuerungX of Ventilsteuerung is

signal resttimeintern: positive := 1;
signal resttimeconstant: positive;
signal done: std_logic := '0';
begin

    
   process(STD)
    begin
    if(rising_edge(STD)) then
    resttimeconstant <= to_integer(SekundenVorgabe);
    end if; 
    end process;

    process(Sek)
    begin
    if(rising_edge(Sek)) then
    if(resttimeconstant>1) then
    if(FreigabeIn='1') then
    if(WIV='1') then
    if(resttimeintern < resttimeconstant +1) then
         resttimeintern <= resttimeintern +1;
         end if;
    end if;
    end if;
    end if;
    end if;
    end process;


        done <= '1' when resttimeintern >= resttimeconstant else 
                '0'; 

        --emv = 0 close
        --emv = 1 is open
        EMV <= '1' when (WIV = '1' or rising_edge(WIV)) and
               (done = '0' or falling_edge(done)) and
               (FreigabeIn = '1' or rising_edge(FreigabeIn)) else '0';


        --frout = 1 : next one can work
        --frout = 0 : work in progress
        FreigabeOut <= '0' when FreigabeIn ='0' else
                       '0' when falling_edge(FreigabeIn) else
                       '1' when rising_edge(done) else
                       '0' when falling_edge(done) else
                       '0' when done ='0' else
                       '1' when done = '1' else
                       '0';

end VentilsteuerungX;
