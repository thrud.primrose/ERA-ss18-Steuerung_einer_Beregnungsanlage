library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Voreinstellung is
    port (
        Bedarf: in std_logic;
        SchalterSekundenEiner: in std_logic_vector(3 downto 0);
        SchalterSekundenZehner: in std_logic_vector(3 downto 0);
        SchalterMinuten: in std_logic_vector(3 downto 0);
        Sekundenvorgabe: out unsigned(11 downto 0)
    );
end entity Voreinstellung;

architecture v1 of Voreinstellung is
    signal SekundenvorgabeIntern: unsigned(11 downto 0) := to_unsigned(0, 12);
begin
    SekundenvorgabeIntern <= resize(unsigned(SchalterSekundenEiner) +
        (unsigned(SchalterSekundenZehner) * to_unsigned(10, 4)) +
        (unsigned(SchalterMinuten) * to_unsigned(60, 6)), 12);
    Sekundenvorgabe <= SekundenvorgabeIntern when Bedarf = '1' else to_unsigned(0, 12);
end architecture v1;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity SummeVoreinstellungen is
    port (
        STD: in std_logic;
        Voreinstellung1: in unsigned(11 downto 0);
        Voreinstellung2: in unsigned(11 downto 0);
        Voreinstellung3: in unsigned(11 downto 0);
        Voreinstellung4: in unsigned(11 downto 0);
        Summe: out unsigned(11 downto 0)
    );
end entity SummeVoreinstellungen;

architecture v1 of SummeVoreinstellungen is
begin

Summe <= Voreinstellung1 + Voreinstellung2 + Voreinstellung3 + Voreinstellung4;
    
end architecture v1;

-- vim:sts=4:sw=4:et:
