#Beregelung einer Wassersteuerungsanlage
#Vorassetzungen
Linux
GHDL
GTKWave

#Info
Nur die "Voreinstellungen" beinhaltet der Teil des Praktikums der von userer Gruppe zu machen war.
Alle anderen .vhdl files haben das Code für andere Komponente des Systems.
thetbv2.vhdlv2.vcd kann in gtkwave weiter untersucht werden.

#Kompilieren
ghdl -a <file_name>.vhdl
ghdl -e <file_name>.vhdl
ghdl -r tbv2

zusätzlich ghdl -r tbv2 --vcd=<file_name>.vcd und gtkwave <file_name>.vcd


