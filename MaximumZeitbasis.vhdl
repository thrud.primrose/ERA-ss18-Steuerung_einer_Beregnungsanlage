library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity MaximumZeitbasis is
	port ( 	
			Einstellung: in Unsigned(1 downto 0);
			--Zeitbasis und Vergleichswert sind Konstanten
			--Jeder Zeitbasis wird ein Vergleichswert zugeordnet
			Zeitbasis: out Unsigned (4 downto 0);
			Vergleichswert: out Unsigned (3 downto 0)
		);
end MaximumZeitbasis;

architecture v1 of MaximumZeitbasis is
signal sEinstellung: Unsigned(1 downto 0):="UU";

begin
sEinstellung <= Einstellung;
Zeitbasis <= "00100" when sEinstellung = "01" else
             "01000" when sEinstellung = "10" else
             "10000";

Vergleichswert <= "1111" when sEinstellung ="01" else
	           "1000" when sEinstellung = "10" else
	           "0100"; --Vergleichswerte sind nicht von der AUf
	           --gabenstellung fixiert	
end v1;





