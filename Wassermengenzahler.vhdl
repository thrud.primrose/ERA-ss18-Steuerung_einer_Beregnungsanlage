library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity Wassermengenzahler is
	port (	--Zeitbasis und Vergleichswert kommen von "Maximum Zeitbasís"
			Zeitbasis: in Unsigned (4 downto 0);
			Vergleichswert: in Unsigned (3 downto 0);

			--Umdehungen kommen vom Wasserrad
         	Umdrehungen: in Unsigned (8 downto 0);

         	--Stunden- und Sekundentakt
        	STD: in std_logic;
         	Sek: in std_logic;

         	--Alarm Signal
         	WIV: out std_logic
         );   
end Wassermengenzahler;

architecture ArchName of Wassermengenzahler is

	--Die Umdrehungszahl zum Zeitpunkt der letzten Berechnung
	signal lastTurns: Unsigned( 8 downto 0) :="000000000";


	--Ein Sekundenzähler
	signal counter: Unsigned(8 downto 0) := "000000000";



	--Ergebnis der Berechnung
	signal quotient: unsigned ( 8 downto 0):="000000001";

	--when wiv := 0 zugewiesen ist dann darf es nicht funktionieren
	signal wivtozero: std_logic:='0'; 


begin

	

	--Nach dem Stundensignal wird der Alarm ausgeschaltet
	--process(STD)
	--begin
	--if rising_edge(STD) then
	--	WIV <= '1';
	--	end if;
	--end process;


	process (Sek,STD)
	--substaraction seems not to work, another way to get 2s complement??
	begin
	if (rising_edge(STD)) then
       WIV <= '1';
	end if;

		if rising_edge(Sek) then
		if(wivtozero='0') then

			--Der Sekundenzähler wird jede Sekunde erhöht
			counter <= counter+1;
			lastTurns <= resize(lastTurns + Umdrehungen, lastTurns'length);
			
			if(counter = Zeitbasis) then
				--Wenn die Sekundenanzahl der angegebenen Zeitbasis vorbei ist, wird der Sekundenzähler wieder auf 0 gesetzt
			   

				if(Zeitbasis = "10000") then
                    quotient <= "0000" & lastTurns(8 downto 4);
				elsif(Zeitbasis ="01000") then
				    quotient <= "000" & lastTurns(8 downto 3);
				else 
				    quotient <= "00" & lastTurns(8 downto 2);
				end if;


				--Es wird uberpruft ob der Umdrehungsspeicher des Wasserrads ubergelaufen, also über 256 gewachsen ist
				--Falls das so ist faengt der Zaehler wieder bei 0 an und es wird eine Zwischenrechnung durchgeführt

				

				 --Falls zu viel Wasser geflossen ist, wird Alarm ausgelöst
				if(quotient>Vergleichswert) then
					WIV <= '0';
					wivtozero <= '1';
				end if;

				lastTurns <= "000000000";
				counter <= to_unsigned(0, counter'length);

			end if;
			end if;
			end if;
	end process;


end ArchName;

