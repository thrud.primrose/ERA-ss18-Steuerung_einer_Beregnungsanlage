library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity thetbv2 is
end entity;

architecture tbv2 of thetbv2 is

component MaximumZeitbasis is 
   port (
    Einstellung: in Unsigned(1 downto 0);
	Zeitbasis: out Unsigned (4 downto 0);
	Vergleichswert: out Unsigned (3 downto 0)
   );
end component MaximumZeitbasis;

signal Einstellung: unsigned(1 downto 0) := "01";
signal Zeitbasis: unsigned(4 downto 0):= "00100";
signal Vergleichswert: unsigned(3 downto 0 ) := "0010";

--

component Wassermengenzahler is
	port (
		Zeitbasis: in Unsigned(4 downto 0);
		Vergleichswert: in Unsigned(3 downto 0);
		Umdrehungen: in Unsigned ( 8 downto 0);
		STD: in std_logic;
		Sek: in std_logic;
		WIV: out std_logic
	);
end component Wassermengenzahler;

signal STD: std_logic := '0';
signal Sek: std_logic := '0';
signal WIV: std_logic :='1';
signal Umdrehungen: Unsigned(8 downto 0) := to_unsigned(24, 9);

component Voreinstellung is
        port (
            Bedarf: in std_logic;
            SchalterSekundenEiner: in std_logic_vector(3 downto 0);
            SchalterSekundenZehner: in std_logic_vector(3 downto 0);
            SchalterMinuten: in std_logic_vector(3 downto 0);
            Sekundenvorgabe: out unsigned(11 downto 0)
        );
    end component Voreinstellung;

    signal Bedarf: std_logic := '1';
    signal Bedarf1: std_logic := '1';
    signal Bedarf2: std_logic := '1';
    signal Bedarf3: std_logic := '1';
    signal Bedarf4: std_logic := '1';
   
component SummeVoreinstellungen is
        port (
            STD: in std_logic;
            Voreinstellung1: in unsigned(11 downto 0);
            Voreinstellung2: in unsigned(11 downto 0);
            Voreinstellung3: in unsigned(11 downto 0);
            Voreinstellung4: in unsigned(11 downto 0);
            --if you want to change time in process you have to instantiate signals 1..4
            --for each corresponding component, otherwise it changes the value for all of 
            --the components!
            Summe: out unsigned(11 downto 0)
        );
    end component SummeVoreinstellungen;

    signal Sekundenvorgabe1: unsigned(11 downto 0);
    signal Sekundenvorgabe2: unsigned(11 downto 0);
    signal Sekundenvorgabe3: unsigned(11 downto 0);
    signal Sekundenvorgabe4: unsigned(11 downto 0);
    signal Summe: unsigned(11 downto 0);

component Ventilsteuerung is
	port (
		SekundenVorgabe : in unsigned(11 downto 0);
		FreigabeIn      : in std_logic;
		WIV             : in std_logic;
		Sek             : in std_logic;
        STD             : in std_logic;
        FreigabeOut     : out std_logic;
        EMV             : out std_logic
	);
end component Ventilsteuerung;

signal FreigabeInHp: std_logic:='0';
signal FreigabeOutHp : std_logic :='0';
signal EMVHp: std_logic:='0';
signal FreigabeOut1: std_logic:='0'; 
signal EMV1: std_logic:='0';
signal FreigabeOut2: std_logic:='0'; 
signal EMV2: std_logic:='0';
signal FreigabeOut3 : std_logic:='0'; 
signal EMV3: std_logic:='0';
signal FreigabeOut4: std_logic:='0'; 
signal EMV4: std_logic:='0';

signal FreigabeIn1: std_logic :='0';
signal FreigabeIn2: std_logic:='0';
signal FreigabeIn3: std_logic:='0';
signal FreigabeIn4: std_logic:='0';

signal instantiate : std_logic := '0';


begin

MaximumZeitbasis_tb: MaximumZeitbasis port map ( Einstellung=>Einstellung, Zeitbasis=>Zeitbasis,
	 Vergleichswert=> Vergleichswert);
Wassermengenzaehler_tb: Wassermengenzahler port map (Zeitbasis => Zeitbasis, Vergleichswert => Vergleichswert,
	Umdrehungen => Umdrehungen, STD => STD, Sek => Sek, WIV => WIV);
Voreinstellung1: Voreinstellung port map (
        Bedarf => Bedarf1,
        SchalterSekundenEiner => "1001",
        SchalterSekundenZehner => "0101",
        SchalterMinuten => "1001",
        Sekundenvorgabe => Sekundenvorgabe1
    );
    Voreinstellung2: Voreinstellung port map (
        Bedarf => Bedarf2,
        SchalterSekundenEiner => "1001",
        SchalterSekundenZehner => "0101",
        SchalterMinuten => "1001",
        Sekundenvorgabe => Sekundenvorgabe2
    );
    Voreinstellung3: Voreinstellung port map (
        Bedarf => Bedarf3,
        SchalterSekundenEiner => "1001",
        SchalterSekundenZehner => "0101",
        SchalterMinuten => "1001",
        Sekundenvorgabe => Sekundenvorgabe3
    );
    Voreinstellung4: Voreinstellung port map (
        Bedarf => Bedarf4,
        SchalterSekundenEiner => "1001",
        SchalterSekundenZehner => "0101",
        SchalterMinuten => "1001",
        Sekundenvorgabe => Sekundenvorgabe4
    );
    Summe1: SummeVoreinstellungen port map (
        STD => STD,
        Voreinstellung1 => Sekundenvorgabe1,
        Voreinstellung2 => Sekundenvorgabe2,
        Voreinstellung3 => Sekundenvorgabe3,
        Voreinstellung4 => Sekundenvorgabe4,
        Summe => Summe
    );
    --freigabein == freigabe out then
    --only when hauptventil offen ist darf die anderen ventile offen sein

    

    --

VentilsteuerungHaupt: entity WORK.Ventilsteuerung(Hauptventilsteuerung) port map (SekundenVorgabe=>Summe, 
	FreigabeIn=>FreigabeInHp, FreigabeOut=>FreigabeOutHp,
	WIV=>WIV, Sek => Sek, STD=>STD);
 
Ventilsteuerung1: entity WORK.Ventilsteuerung(VentilsteuerungX) port map (SekundenVorgabe=>SekundenVorgabe1, 
	FreigabeIn => FreigabeOutHp, 	WIV=>WIV, Sek => Sek, STD=>STD, FreigabeOut=>FreigabeOut1);

Ventilsteuerung2: entity WORK.Ventilsteuerung(VentilsteuerungX) port map (SekundenVorgabe=>SekundenVorgabe2, 
	FreigabeIn => FreigabeOut1, FreigabeOut => FreigabeOut2,
	WIV=>WIV, Sek => Sek, STD=>STD);

Ventilsteuerung3: entity WORK.Ventilsteuerung(VentilsteuerungX) port map (SekundenVorgabe=>SekundenVorgabe3, 
	FreigabeIn => FreigabeOut2, FreigabeOut => FreigabeOut3,
	WIV=>WIV, Sek => Sek, STD=>STD);
Ventilsteuerung4: entity WORK.Ventilsteuerung(VentilsteuerungX) port map (SekundenVorgabe=>SekundenVorgabe4, 
	FreigabeIn => FreigabeOut3,
	WIV=>WIV, Sek => Sek, STD=>STD);

Sek <= not Sek after 0.5 sec;



--stdprocess
process
begin 
STD <= '0';
FreigabeInHp <= '1';

--FreigabeIn1 <= '0';
--EMV1<= '0';
--FreigabeOut1 <='0';
--FreigabeIn2 <= '0';
--EMV2 <= '0';
--FreigabeOut2 <='0';
--FreigabeIn3 <= '0';
--EMV3 <= '0';
--FreigabeOut3 <='0';
--FreigabeIn4 <= '0';
--EMV4 <= '0';
--FreigabeOut4 <='0';


wait for 5 sec;


--liste vordefinierter signale
--sec, clk, freigabeinhauptventil, sekundeneingaben durch schlatere, 
--umdrehungen
STD <= '1';



--FreigabeInHp <= '1';
wait for 1 min;
STD <= '0';
wait for 58 min;
wait for 5 sec;
end process; 

process 
begin 

--ohne probleme
Einstellung <= "01";

--WIV must be 1 after this
Umdrehungen <= to_unsigned(4, 9);
wait for 20 sec;

end process;

end architecture tbv2;
